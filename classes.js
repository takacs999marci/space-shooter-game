class Game {
    constructor(x, y, width, height, color, over, score) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.over = over;
        this.score = score;
    }
    draw() {
        ctx.fillStyle = this.color;
        ctx.beginPath();
        ctx.fillRect(this.x, this.y, this.width, this.height);
        ctx.closePath();
        ctx.fill()
    }
}

class Rect {
    constructor(x, y, width, height, speed, color) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.speed = speed;
        this.color = color;
    }
    draw() {
        ctx.fillStyle = this.color;
        ctx.beginPath();
        ctx.fillRect(this.x, this.y, this.width, this.height);
        ctx.closePath();
        ctx.fill()
    }
}

class Circle {
    constructor(x, y, radius, color, speed) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.speed = speed;
    }
    draw() {
        ctx.fillStyle = this.color;
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, true);
        ctx.closePath();
        ctx.fill()
    }
}

class Str {
    constructor(x, y, text, size, font, color){
        this.x = x;
        this.y = y;
        this.text = text;
        this.size = size;
        this.font = font;
        this.color = color;
    }
    draw(){
  
        ctx.font = this.size+" "+this.font;
        ctx.fillStyle = this.color;
        ctx.fillText(this.text, this.x, this.y, 1000);

    }
}