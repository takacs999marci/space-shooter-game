function move_overlay(id, dir) {
    if (dir == 'right') {
        document.getElementsByClassName('overlay')[id].style = "transform: rotateZ(30deg) translate(400px,-200px); transition-duration: .9s;";
    }
    else if (dir == 'left') {
        document.getElementsByClassName('overlay')[id].style = "transform: rotateZ(30deg) translate(0, 0); transition-duration: .6s;";
    }
}

function power_on_off(){

   let splash =  document.getElementById('splash_screen');
   let menu = document.getElementById('menu');
    let canvas = document.querySelector('canvas');

    if (!clicked) {
        document.body.style.backgroundImage = "url('img/tv2.png')";
        clicked = true;
        splash.style.opacity = 0;
        menu.style.opacity = 0;
        canvas.style.opacity = 0;
    }
    else{
        document.body.style.backgroundImage = "url('img/tv.png')";
        clicked = false;
        splash.style.opacity = 1;
        menu.style.opacity = 1;
        canvas.style.opacity = 1;
    }
   
}

function spacebar(){
    document.getElementById('spacebar').classList.add('button_press');
    setTimeout(() => {
        document.getElementById('spacebar').classList.remove('button_press');    
    }, 100);
    create_rocket();
}

function left_arrow() {
    document.getElementById('left_arrow').classList.add('button_press');
    setTimeout(() => {
        document.getElementById('left_arrow').classList.remove('button_press');
    }, 100);
    if (velX > -player.speed) {
        velX-=2;
    }
}

function right_arrow() {
    document.getElementById('right_arrow').classList.add('button_press');
    setTimeout(() => {
        document.getElementById('right_arrow').classList.remove('button_press');
    }, 100);
    if (velX < player.speed) {
        velX+=2;
    }
}

function up_arrow() {
    document.getElementById('up_arrow').classList.add('button_press');
    setTimeout(() => {
        document.getElementById('up_arrow').classList.remove('button_press');
    }, 100);
    if (velY > -player.speed) {
        velY-=2;
    }
}

function down_arrow() {
    document.getElementById('down_arrow').classList.add('button_press');
    setTimeout(() => {
        document.getElementById('down_arrow').classList.remove('button_press');
    }, 100);
    if (velY < player.speed) {
        velY+=2;
    }
}


addEventListener("keydown", function (e) {
    if(e === 122){
        document.getElementById('fullscreen').style = "display: none";
        document.getElementById('splash_screen').style = "display: block";
    }

});



function openFullscreen() {
    let elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    }
    else if (elem.webkitRequestFullscreen) { /* Safari */
        elem.webkitRequestFullscreen();
    }
    else if (elem.msRequestFullscreen) { /* IE11 */
        elem.msRequestFullscreen();
    }
    document.getElementById('fullscreen').style = "display: none";
    document.getElementById('splash_screen').style = "display: block";
}

function navigate_somewhere(){
    places = ["https://www.playngo.com", "https://www.linkedin.com/in/marcell-takacs/"];
    window.location.href = places[Math.round(Math.random())];
}

function create_enemy() {
        enemy = new Rect(800, Math.floor(Math.random() * 501) + 50, 50, 50, 10, "red");
        enemies.push(enemy);
        console.log(enemies);
}
function create_rocket(){

    if (rockets.length == 0) {
        rocket = new Circle(player.x + player.width, player.y + (player.height / 2), 10, "lime", 15);
        rockets.push(rocket);
        console.log(rockets);
    }

}

function create_explosion(x, y, radius, color){
    explosion = new Circle(x, y, radius, color, 0);
    explosion.draw();
}

function create_stars(num, moving, speed) {
    for (let i = 0; i < num; i++) {
        star = new Circle(Math.floor((Math.random() * 800) + 1), Math.floor((Math.random() * 600) + 1), Math.floor((Math.random() * 3) + 0.1), "white", speed);
        if(moving){
            moving_stars.push(star);
        }
        else{
            stars.push(star);
        }
    }
}

function create_particles(x, y){

            particle = new Rect(x, y, enemy.width / 2, enemy.height / 2, 20, "red");
            particle2 = new Rect(x + enemy.width / 2, y, enemy.width / 2, enemy.height / 2, 20, "red");
            particle3 = new Rect(x, y - enemy.height / 2, enemy.width / 2, enemy.height / 2, 20, "red");
            particle4 = new Rect(x + enemy.width / 2, y - enemy.height / 2, enemy.width / 2, enemy.height / 2, 20, "red");
            particles.push(particle, particle2, particle3, particle4);

}
function draw_particles(){ //EZ MÉG NEM OKÉS
    for (let i = 0; i < particles.length; i++) {
        particles[i].draw();
        if (particles[i].y >= 0 & particles[i].x >= 0 & particles[i].x <= 800 & particles[i].y <= 600) {
            
            if (particles[i] === particle) {
                particles[i].x -= particle.speed;
                particles[i].y -= particle.speed;
            }
            else if (particles[i] === particle2) {
                particles[i].x += particle.speed;
                particles[i].y -= particle.speed;
            }
            else if (particles[i] === particle3) {
                particles[i].x -= particle.speed;
                particles[i].y += particle.speed;
            }
            else if (particles[i] === particle4) {
                particles[i].x += particle.speed;
                particles[i].y += particle.speed;
            } 
        }
        else{
            particles.splice(i, 1);
            
        }

        
    }
}

function player_movement(){
    if (keys[38]) {
        if (velY > -player.speed) {
            velY--;
        }
    }

    if (keys[40]) {
        if (velY < player.speed) {
            velY++;
        }
    }
    if (keys[39]) {
        if (velX < player.speed) {
            velX++;
        }
    }
    if (keys[37]) { 
        if (velX > -player.speed) {
            velX--;
        }
    }

    if (keys[32]) {
        create_rocket();            
    }

    velY *= friction;
    player.y += velY;
    velX *= friction;
    player.x += velX;

    if (player.x >= game.width - player.width) {
        player.x = game.width - player.width;
    } else if (player.x <= 0) {
        player.x = 0;
    }

    if (player.y > game.height - player.height) {
        player.y = game.height - player.height;
    } else if (player.y <= 0) {
        player.y = 0;
    }
}

function draw_stars() {
    for (let i = 0; i < stars.length; i++) {
        stars[i].draw();

    }

    for (let i = 0; i < moving_stars.length; i++) {
        moving_stars[i].draw();
        moving_stars[i].x += moving_stars[i].speed;
        if (moving_stars[i].x < 0) {
            moving_stars[i].x = 805;
            moving_stars[i].y = Math.floor((Math.random() * 600) + 1);
        }

    }
}
function draw_rockets() {
    for (var i = 0; i < rockets.length; i++) {
        rockets[i].draw();
        rockets[i].x += rocket.speed;
        if (rockets[i].x >= 800) {
            rockets.splice(i, 1);
        }
    }
}

function draw_player() {
    player.draw(); 
}

function draw_enemies() {

    for (var i = 0; i < enemies.length; i++) {
        
        const disX = player.x+(player.width/2) - enemies[i].x + (enemies[i].width/2);
        const disY = player.y+(player.height/2) - enemies[i].y + (enemies[i].height/2);
        
        if (rockets.length > 0) {
            const distanceX = rocket.x - enemies[i].x;
            const distanceY = rocket.y - enemies[i].y;
            if (rockets.length > 0 & player.x < enemies[i].x & distanceX >= 0 & distanceY >= -10 & distanceY <= 60) {
                create_explosion(rocket.x, rocket.y, 70, "red");
                create_particles(rocket.x, rocket.y);
                enemies.splice(i, 1);
                rockets.splice(0, 1);
                game.score += 10;
            }
        }

        if (enemies[i].x <= -50) {
            enemies.splice(i, 1);
            game.score -= 5;
            if(game.score <= 0){
                game.score = 0;
            }
        }
        if (player.x < (enemies[i].x + enemies[i].width) & disX >= -10 & disY >= 12 & disY <= 85){
            create_explosion(player.x + (player.width / 2), player.y + (player.height / 2), 80, "blue");
            game.over = true;      

        }
        else {
            enemies[i].draw();
            enemies[i].x -= 4;

            setTimeout(() => {
                enemies[i].y -= Math.round(Math.random()) * 2 - 1;           
            }, 1000);
 
        }
    }
}

function draw_score(x, y) {
    str = new Str(x, y, "Score: " + game.score, "50px", "arial", "red");
    str.draw();
}