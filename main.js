const CANVAS = document.querySelector('CANVAS');
CANVAS.width = 800;
CANVAS.height = 600;
const ctx = CANVAS.getContext('2d');

var clicked = false,
    velY = 0,
    velX = 0,
    friction = 0.9,
    keys = [],
    rockets = [],
    enemies = [],
    stars = [],
    moving_stars = [],
    particles = [];

function start() {

    document.getElementById("menu").style.display = "none";
    document.getElementById("splash_screen").style.display = "none";

    player = new Rect(100, 300, 70, 30, 10, '#00a');
    game = new Game(0, 0, CANVAS.width, CANVAS.height, 'black', false, 0);
    gameover_text = new Str(150, 300, "GAME OVER", "80px", "arial", "red");
    
     drawLoop = setInterval(update, 1000/60);
    if (!game.over) {
         enemyLoop = setInterval(create_enemy, 2000);

    }
    setInterval(function(){game.score+=1;}, 1000);
    create_stars(300, false, 0);
    create_stars(150, true, -1);
    create_stars(150, true, -0.5);
    create_stars(150, true, -0.25);
    create_stars(150, true, -0.1);
}

function update() {

    game.draw();
    if (game.over) {

        gameover_text.draw();
        draw_score(300,400);
        clearInterval(drawLoop);
        clearInterval(enemyLoop);
       
        setTimeout(() => {
            location.reload();
        }, 2000);
        
    }
    else{
 
        draw_stars();
        draw_player();
        draw_rockets();
        draw_particles();
        draw_enemies();
        draw_score(50, 70);
       
        player_movement();
    }
}

addEventListener("keydown", function (e) {
    keys[e.keyCode] = true;

});
addEventListener("keyup", function (e) {
    keys[e.keyCode] = false;
});